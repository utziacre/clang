#ifdef GET_SME_IMMEDIATE_CHECK
case SME::BI__builtin_sme_svld1_hor_vnum_za128:
ImmChecks.push_back(std::make_tuple(0, 17, 0));
ImmChecks.push_back(std::make_tuple(2, 16, 0));
  break;
case SME::BI__builtin_sme_svld1_hor_vnum_za16:
ImmChecks.push_back(std::make_tuple(0, 13, 0));
ImmChecks.push_back(std::make_tuple(2, 6, 0));
  break;
case SME::BI__builtin_sme_svld1_hor_vnum_za32:
ImmChecks.push_back(std::make_tuple(0, 15, 0));
ImmChecks.push_back(std::make_tuple(2, 15, 0));
  break;
case SME::BI__builtin_sme_svld1_hor_vnum_za64:
ImmChecks.push_back(std::make_tuple(0, 6, 0));
ImmChecks.push_back(std::make_tuple(2, 13, 0));
  break;
case SME::BI__builtin_sme_svld1_hor_vnum_za8:
ImmChecks.push_back(std::make_tuple(0, 16, 0));
ImmChecks.push_back(std::make_tuple(2, 17, 0));
  break;
case SME::BI__builtin_sme_svld1_hor_za128:
ImmChecks.push_back(std::make_tuple(0, 17, 0));
ImmChecks.push_back(std::make_tuple(2, 16, 0));
  break;
case SME::BI__builtin_sme_svld1_hor_za16:
ImmChecks.push_back(std::make_tuple(0, 13, 0));
ImmChecks.push_back(std::make_tuple(2, 6, 0));
  break;
case SME::BI__builtin_sme_svld1_hor_za32:
ImmChecks.push_back(std::make_tuple(0, 15, 0));
ImmChecks.push_back(std::make_tuple(2, 15, 0));
  break;
case SME::BI__builtin_sme_svld1_hor_za64:
ImmChecks.push_back(std::make_tuple(0, 6, 0));
ImmChecks.push_back(std::make_tuple(2, 13, 0));
  break;
case SME::BI__builtin_sme_svld1_hor_za8:
ImmChecks.push_back(std::make_tuple(0, 16, 0));
ImmChecks.push_back(std::make_tuple(2, 17, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_vnum_za128:
ImmChecks.push_back(std::make_tuple(0, 17, 0));
ImmChecks.push_back(std::make_tuple(2, 16, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_vnum_za16:
ImmChecks.push_back(std::make_tuple(0, 13, 0));
ImmChecks.push_back(std::make_tuple(2, 6, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_vnum_za32:
ImmChecks.push_back(std::make_tuple(0, 15, 0));
ImmChecks.push_back(std::make_tuple(2, 15, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_vnum_za64:
ImmChecks.push_back(std::make_tuple(0, 6, 0));
ImmChecks.push_back(std::make_tuple(2, 13, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_vnum_za8:
ImmChecks.push_back(std::make_tuple(0, 16, 0));
ImmChecks.push_back(std::make_tuple(2, 17, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_za128:
ImmChecks.push_back(std::make_tuple(0, 17, 0));
ImmChecks.push_back(std::make_tuple(2, 16, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_za16:
ImmChecks.push_back(std::make_tuple(0, 13, 0));
ImmChecks.push_back(std::make_tuple(2, 6, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_za32:
ImmChecks.push_back(std::make_tuple(0, 15, 0));
ImmChecks.push_back(std::make_tuple(2, 15, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_za64:
ImmChecks.push_back(std::make_tuple(0, 6, 0));
ImmChecks.push_back(std::make_tuple(2, 13, 0));
  break;
case SME::BI__builtin_sme_svld1_ver_za8:
ImmChecks.push_back(std::make_tuple(0, 16, 0));
ImmChecks.push_back(std::make_tuple(2, 17, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_vnum_za128:
ImmChecks.push_back(std::make_tuple(0, 17, 0));
ImmChecks.push_back(std::make_tuple(2, 16, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_vnum_za16:
ImmChecks.push_back(std::make_tuple(0, 13, 0));
ImmChecks.push_back(std::make_tuple(2, 6, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_vnum_za32:
ImmChecks.push_back(std::make_tuple(0, 15, 0));
ImmChecks.push_back(std::make_tuple(2, 15, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_vnum_za64:
ImmChecks.push_back(std::make_tuple(0, 6, 0));
ImmChecks.push_back(std::make_tuple(2, 13, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_vnum_za8:
ImmChecks.push_back(std::make_tuple(0, 16, 0));
ImmChecks.push_back(std::make_tuple(2, 17, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_za128:
ImmChecks.push_back(std::make_tuple(0, 17, 0));
ImmChecks.push_back(std::make_tuple(2, 16, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_za16:
ImmChecks.push_back(std::make_tuple(0, 13, 0));
ImmChecks.push_back(std::make_tuple(2, 6, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_za32:
ImmChecks.push_back(std::make_tuple(0, 15, 0));
ImmChecks.push_back(std::make_tuple(2, 15, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_za64:
ImmChecks.push_back(std::make_tuple(0, 6, 0));
ImmChecks.push_back(std::make_tuple(2, 13, 0));
  break;
case SME::BI__builtin_sme_svst1_hor_za8:
ImmChecks.push_back(std::make_tuple(0, 16, 0));
ImmChecks.push_back(std::make_tuple(2, 17, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_vnum_za128:
ImmChecks.push_back(std::make_tuple(0, 17, 0));
ImmChecks.push_back(std::make_tuple(2, 16, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_vnum_za16:
ImmChecks.push_back(std::make_tuple(0, 13, 0));
ImmChecks.push_back(std::make_tuple(2, 6, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_vnum_za32:
ImmChecks.push_back(std::make_tuple(0, 15, 0));
ImmChecks.push_back(std::make_tuple(2, 15, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_vnum_za64:
ImmChecks.push_back(std::make_tuple(0, 6, 0));
ImmChecks.push_back(std::make_tuple(2, 13, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_vnum_za8:
ImmChecks.push_back(std::make_tuple(0, 16, 0));
ImmChecks.push_back(std::make_tuple(2, 17, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_za128:
ImmChecks.push_back(std::make_tuple(0, 17, 0));
ImmChecks.push_back(std::make_tuple(2, 16, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_za16:
ImmChecks.push_back(std::make_tuple(0, 13, 0));
ImmChecks.push_back(std::make_tuple(2, 6, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_za32:
ImmChecks.push_back(std::make_tuple(0, 15, 0));
ImmChecks.push_back(std::make_tuple(2, 15, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_za64:
ImmChecks.push_back(std::make_tuple(0, 6, 0));
ImmChecks.push_back(std::make_tuple(2, 13, 0));
  break;
case SME::BI__builtin_sme_svst1_ver_za8:
ImmChecks.push_back(std::make_tuple(0, 16, 0));
ImmChecks.push_back(std::make_tuple(2, 17, 0));
  break;
#endif

